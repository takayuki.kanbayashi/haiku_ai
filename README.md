# AIで俳句を読む

## ファイルと説明

|ファイルパス|説明|
|-|-|
| ./dataset/issa.csv | 小林一茶の俳句や年月日の情報が入ったのデータ |
| ./dataset/issa19.csv | 小林一茶の俳句や年月日の情報が入ったのデータ |
| ./dataset/issa.csv | 小林一茶の俳句のみのデータ |
| ./dataset/kotoba_number.csv | 「あ,い,・・・ん」までの言葉のナンバリング対応表 |
| ./dataset/dataset.csv | ナンバリング表記の俳句データ |
| ./dataset/haik_list.pickle | onehot　label化した俳句データ |
| ./dataset/make_dataset.ipynb | データセットをonehot label化するjupyter-notebook |

&nbsp;

|ファイルパス|説明|
|-|-|
| ./src/AutoEncoder.py | 俳句データをオートエンコーダして学習を実行するモデル |
| ./src/DatasetLoders/DatasetLoder.py | 学習用データセットを読み込む専用のクラス |
| ./src/DatasetLoders/MakeDataset.py | 学習用データセットを作成する専用のクラス(tensor型で) |
| ./src/ModelClasses/ModelClass.py | 学習モデルの定義専用クラス |

&nbsp;

|ファイルパス|説明|
|-|-|
| ./sandbox/sandbox.py | lambdaの使用理解 |

## 作る学習モデル

### AutoEncoder
学習モデルが俳句の特徴を見つけられるかを評価する為に作成。

### VAE-CNN
中間層でガウス分布に従う乱数を入れることで、オリジナルの俳句データを生成することが出来る。

### RNN
時系列モデルなので、「あ、い、う、〜、ん」の50音を先頭の入力として残り16字を自動生成させる

### LSTM
時系列モデルなので、「あ、い、う、〜、ん」の50音を先頭の入力として残り16字を自動生成させる