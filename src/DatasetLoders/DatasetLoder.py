import pandas as pd
import numpy as np
import torch
import pickle

class LoadHaiku_Dataset():
    def __init__(self, filepath):
        self.filepath=filepath
    
    def Load(self, onehot=True):
        self.onehot = onehot

        with open(self.filepath, "rb") as f:
            self.haiku_data = pickle.load(f)
        
        if self.onehot:
            self.tensor_haiku_data = torch.from_numpy(np.array(self.haiku_data)).clone()
            self.onehot_haiku_data = torch.eye(89)[self.tensor_haiku_data]
            return self.onehot_haiku_data
        else:
            self.tensor_haiku_data = torch.from_numpy(np.array(self.haiku_data)).clone()
            self.onehot_haiku_data = None
            return self.tensor_haiku_data
    
    def PrintData(self, n=0):
        if not self.onehot_haiku_data is None:
            print(f"tensor : {self.tensor_haiku_data}")
            print(f"onehot : {self.onehot_haiku_data}")
        else:
            print(f"tensor : {self.tensor_haiku_data}")



        

