# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from ModelClasses import ModelClass
import pandas as pd
import numpy as np
from DatasetLoders import DatasetLoder

"""

"""
class MakeAutoEncoder_Dataset(torch.utils.data.Dataset):
    def __init__(self, filepath=None, onehot=True, transform=None):

        if filepath is None:
            self.filepath = "../dataset/haiku_list.pickle"
        else:
            self.filepath = filepath

        self.onehot = onehot
        self.transform = transform

        # road datasets
        DL = DatasetLoder.LoadHaiku_Dataset(self.filepath, )
        self.in_dataset = DL.Load(self.onehot)
        self.out_dataset = DL.Load(self.onehot)

        # reshape dataset
        if self.transform:
            self.data_ =  self.transform(torch.stack([self.in_dataset, self.out_dataset], 1))
        else:
            self.data_ = torch.stack([self.in_dataset, self.out_dataset], 1)

        self.datalen = len(self.data_)
    
    def __len__(self):
        return len(self.data_)

    def __getitem__(self, idx):
        return self.data_[:, 0], self.data_[:, 1]
    
    def split_data(self, r=0.9):
        split_num = (int)(self.__len__() * r)

        MAED = lambda : MakeAutoEncoder_Dataset(filepath=self.filepath, onehot=self.onehot)
        ds1 = MAED()
        ds1.data_ = ds1.data_[:split_num]

        ds2 = MAED()
        ds2._data = ds2.data_[split_num:]

        return ds1, ds2





