import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms

# AutoEncoder Model
class AutoEncoder1(nn.Module):
    def __init__(self, INPUT_FEATURES=1869, OUTPUT_RESULTS=1869):
        # 定数（モデル定義時に必要となるもの）
        self.INPUT_FEATURES = INPUT_FEATURES# 入力（特徴）の数： INPUT_FEATURES
        self.LAYER1_NEURONS = 1024      # ニューロンの数： 1024
        self.LAYER2_NEURONS = 512      # ニューロンの数： 512
        self.LAYER3_NEURONS = 1024      # ニューロンの数： 1024
        self.OUTPUT_RESULTS = OUTPUT_RESULTS# 出力結果の数： OUTPUT_RESULTS

        super(AutoEncoder1, self).__init__()
        # 入力層→中間層①：1つ目のレイヤー（layer）
        self.layer1 = nn.Linear(
            self.INPUT_FEATURES,                # 入力ユニット数：INPUT_FEATURES
            self.LAYER1_NEURONS)                # 次のレイヤーの出力ユニット数：1024

        # 中間層①→中間層②：2つ目のレイヤー（layer）
        self.layer2 = nn.Linear(
            self.LAYER1_NEURONS,                # 入力ユニット数：1024
            self.LAYER2_NEURONS)                # 次のレイヤーへの出力ユニット数：512
        
        # 中間層②→中間層③：3つ目のレイヤー（layer）
        self.layer3 = nn.Linear(
            self.LAYER2_NEURONS,                # 入力ユニット数：512
            self.LAYER3_NEURONS)                # 出力結果への出力ユニット数：1024

        # 中間層③→出力層：4つ目のレイヤー（layer）
        self.layer_out = nn.Linear(
            self.LAYER3_NEURONS,                # 入力ユニット数：1024
            self.OUTPUT_RESULTS)                # 出力結果への出力ユニット数：OUTPUT_RESULTS

    def forward(self, x):
        x_ = []
        # フィードフォワードを定義
        # 「出力＝活性化関数（第n層（入力））」の形式で記述する
        x = x.view(-1, self.INPUT_FEATURES) # 21×89の配列を1869の一次元配列に変換
        x = F.relu(self.layer1(x))  # 活性化関数はreluとして定義
        x = F.relu(self.layer2(x))  # 同上 
        x = F.relu(self.layer3(x))  # 同上
        x = self.layer_out(x) 
        x = x.view(-1, 21)
        for i in range(0, len(x)):
            x_.append(F.softmax(x[i], dim=1)) # 出力する際はsoftmaxを使用
        return x